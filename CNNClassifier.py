from datetime import time

import numpy as np
import tensorflow as tf
from six.moves import cPickle as pickle
from six.moves import range
from DNNClassifier import reformat, num_steps

pickle_file = 'notMNIST.pickle'
with open(pickle_file, 'rb') as f:
    save = pickle.load(f)
    train_dataset = save['train_dataset']
    train_labels = save['train_labels']
    validation_dataset = save['valid_dataset']
    validation_labels = save['valid_labels']
    test_dataset = save['test_dataset']
    test_labels = save['test_labels']
    del save  # hint to help gc free up memory
    print('Training set', train_dataset.shape, train_labels.shape)
    print('Validation set', validation_dataset.shape, validation_labels.shape)
    print('Test set', test_dataset.shape, test_labels.shape)

image_size = 28
num_labels = 10
num_channels = 1
SEED = 1111
batch_size = 128
eval_batch_size = 500

def data_type():
    """Return the type of the activations, weights, and placeholder variables."""
    return tf.float32


# print(train_dataset[0][0:10])

pert_arr = np.random.rand(28, 28) / 5
# print(pert_arr)

for item in (train_dataset):
    for i in range(28):
        for j in range(28):
            item[i][j] = item[i][j] + pert_arr[i][j]

# print(train_dataset[0][0:10])


def reformat_back(dataset, labels):
    dataset = dataset.reshape((-1, image_size, image_size,1)).astype(np.float32)
    # Map 0 to [1.0, 0.0, 0.0 ...], 1 to [0.0, 1.0, 0.0 ...]
    labels = (np.arange(num_labels) == labels[:, None]).astype(np.float32)
    return dataset, labels


train_dataset, train_labels = reformat(train_dataset, train_labels)
validation_dataset, validation_labels = reformat(validation_dataset, validation_labels)
test_dataset, test_labels = reformat(test_dataset, test_labels)
print('Training set', train_dataset.shape, train_labels.shape)
print('Validation set', validation_dataset.shape, validation_labels.shape)
print('Test set', test_dataset.shape, test_labels.shape)
train_size = train_labels.shape[0]


def accuracy(predictions, labels):
    return (100.0 * np.sum(np.argmax(predictions, 1) == np.argmax(labels, 1))
            / predictions.shape[0])


num_input = image_size * image_size
lambda_ = 0.001
keep_prob = 0.8
init_learn_rate = 0.8
decay_steps = 100
decay_rate = 0.99
eval_freq = 500

cnn_graph = tf.Graph()
with cnn_graph.as_default():
    initializer = tf.contrib.layers.xavier_initializer(uniform=True, seed=None, dtype=tf.float32)

    tf_train_dataset = tf.placeholder(data_type(), shape=(batch_size, image_size, image_size, num_channels))
    tf_train_labels = tf.placeholder(tf.int64, shape=(batch_size,))
    tf_validation_dataset = tf.placeholder(data_type(), shape=(eval_batch_size, image_size, image_size, num_channels))
    tf_validation_labels = tf.placeholder(data_type(), shape=(eval_batch_size, image_size, image_size, num_channels))
    tf_test_dataset = tf.placeholder(data_type(),shape=(1500, image_size, image_size, num_channels))
    tf_test_labels = tf.placeholder(data_type(),shape=(1500, image_size, image_size, num_channels))


    train_dataset, train_labels = reformat_back(train_dataset, train_labels)
    validation_dataset, validation_labels = reformat_back(validation_dataset, validation_labels)
    test_dataset, test_labels = reformat_back(test_dataset, test_labels)

    # Variables/weights
    conv1_weights = tf.Variable(
        tf.truncated_normal([5, 5, num_channels, 32],  # 5x5 filter, depth 32.
                            stddev=0.1,
                            seed=SEED, dtype=data_type()))
    conv1_biases = tf.Variable(tf.zeros([32], dtype=data_type()))
    conv2_weights = tf.Variable(tf.truncated_normal(
        [5, 5, 32, 64], stddev=0.1,
        seed=SEED, dtype=data_type()))
    conv2_biases = tf.Variable(tf.constant(0.1, shape=[64], dtype=data_type()))
    fc1_weights = tf.Variable(  # fully connected, depth 512.
        tf.truncated_normal([image_size // 4 * image_size // 4 * 64, 512],
                            stddev=0.1,
                            seed=SEED,
                            dtype=data_type()))
    fc1_biases = tf.Variable(tf.constant(0.1, shape=[512], dtype=data_type()))
    fc2_weights = tf.Variable(tf.truncated_normal([512, num_labels],
                                                  stddev=0.1,
                                                  seed=SEED,
                                                  dtype=data_type()))
    fc2_biases = tf.Variable(tf.constant(
        0.1, shape=[num_labels], dtype=data_type()))


    # Training computation.
    def neural_net(x, drop=False):
        """The neural_net definition."""
        # 2D convolution, with 'SAME' padding (i.e. the output feature map has
        # the same size as the input). Note that {strides} is a 4D array whose
        # shape matches the data layout: [image index, y, x, depth].
        conv = tf.nn.conv2d(x,conv1_weights,strides=[1, 1, 1, 1],padding='SAME')
        # Bias and rectified linear non-linearity.
        relu = tf.nn.relu(tf.nn.bias_add(conv, conv1_biases))
        # Max pooling. The kernel size spec {ksize} also follows the layout of
        # the data. Here we have a pooling window of 2, and a stride of 2.
        pool = tf.nn.max_pool(relu,ksize=[1, 2, 2, 1],strides=[1, 2, 2, 1],padding='SAME')
        conv = tf.nn.conv2d(pool,conv2_weights,strides=[1, 1, 1, 1],padding='SAME')
        relu = tf.nn.relu(tf.nn.bias_add(conv, conv2_biases))
        pool = tf.nn.max_pool(relu,ksize=[1, 2, 2, 1],strides=[1, 2, 2, 1],padding='SAME')
        # Reshape the feature map cuboid into a 2D matrix to feed it to the
        # fully connected layers.
        pool_shape = pool.get_shape().as_list()
        reshape = tf.reshape(pool,[pool_shape[0], pool_shape[1] * pool_shape[2] * pool_shape[3]])
        # Fully connected layer. Note that the '+' operation automatically
        # broadcasts the biases.
        hidden = tf.nn.relu(tf.matmul(reshape, fc1_weights) + fc1_biases)
        # Add dropout during training only. Dropout also scales
        # activations such that no rescaling is needed at evaluation time.
        if drop:
            hidden = tf.nn.dropout(hidden, 0.5, seed=SEED)
        return tf.matmul(hidden, fc2_weights) + fc2_biases


    logits = neural_net(tf_train_dataset, True)
    loss = tf.reduce_mean(tf.nn.sparse_softmax_cross_entropy_with_logits(
        labels=tf_train_labels, logits=logits))

    # L2 regularization for the fully connected parameters.
    regularizers = (tf.nn.l2_loss(fc1_weights) + tf.nn.l2_loss(fc1_biases) +
                    tf.nn.l2_loss(fc2_weights) + tf.nn.l2_loss(fc2_biases))
    # Add the regularization term to the loss.
    loss += 5e-4 * regularizers

    # Optimizer: set up a variable that's incremented once per batch and
    # controls the learning rate decay.
    batch = tf.Variable(0, dtype=data_type())
    global_step = tf.Variable(0)  # count the number of steps taken.
    learning_rate = tf.train.exponential_decay(init_learn_rate, global_step, decay_steps, decay_rate)
    # Use simple momentum for the optimization.
    optimizer = tf.train.MomentumOptimizer(learning_rate,
                                           0.9).minimize(loss,
                                                         global_step=batch)

    # Predictions for the current training minibatch.
    train_prediction = tf.nn.softmax(logits)

    # Predictions for the test and validation, which we'll compute less often.
    eval_prediction = tf.nn.softmax(neural_net(tf_validation_dataset))
    test_prediction = tf.nn.softmax(neural_net(tf_test_dataset))


def eval_in_batches(data, sess):
    """Get all predictions for a dataset by running it in small batches."""
    size = data.shape[0]
    if size < eval_batch_size:
      raise ValueError("batch size for evals larger than dataset: %d" % size)
    predictions = np.ndarray(shape=(size, num_labels), dtype=np.float32)
    for begin in tf.xrange(0, size, eval_batch_size):
      end = begin + eval_batch_size
      if end <= size:
        predictions[begin:end, :] = sess.run(
            eval_prediction,
            feed_dict={validation_dataset: data[begin:end, ...]})
      else:
        batch_predictions = sess.run(
            eval_prediction,
            feed_dict={validation_dataset: data[-eval_batch_size:, ...]})
        predictions[begin:, :] = batch_predictions[begin - size:, :]
    return predictions


config2 = tf.ConfigProto()
config2.gpu_options.allocator_type = 'BFC'

with tf.Session(graph=cnn_graph, config=config2) as session:
    # Run all the initializers to prepare the trainable parameters.
    tf.global_variables_initializer().run()
    print('Initialized!')
    # Loop through training steps.
    for step in range(int(num_steps)):
        # Compute the offset of the current minibatch in the data.
        # Note that we could use better randomization across epochs.
        offset = (step * batch_size) % (train_labels.shape[0] - batch_size)

        batch_data = train_dataset[offset:(offset + batch_size), ...]
        batch_labels = train_labels[offset:(offset + batch_size)]
        # This dictionary maps the batch data (as a numpy array) to the
        # node in the graph it should be fed to.
        feed_dict = {tf_train_dataset: batch_data,
                     tf_train_labels: batch_labels}
        # Run the optimizer to update weights.
        session.run(optimizer, feed_dict=feed_dict)
        # print some extra information once reach the evaluation frequency
        if step % eval_freq == 0:
            # fetch some extra nodes' data
            l, lr, predictions = session.run([loss, learning_rate, train_prediction],
                                          feed_dict=feed_dict)
            elapsed_time = time.time() - start_time
            start_time = time.time()
            print('Step %d (epoch %.2f), %.1f ms' %
            (step, float(step) * batch_size / train_size,
             1000 * elapsed_time / eval_freq))
            print('Minibatch loss: %.3f, learning rate: %.6f' % (l, lr))
            print('Minibatch error: %.1f%%' % accuracy(predictions, batch_labels))
            print('Validation error: %.1f%%' % accuracy(
                eval_in_batches(validation_dataset, session), validation_labels))
            tf.sys.stdout.flush()
    # Finally print the result!
    test_error = accuracy(eval_in_batches(test_dataset, session), test_labels)
    print('Test error: %.1f%%' % test_error)
    # if FLAGS.self_test:
    #     print('test_error', test_error)
    #     assert test_error == 0.0, 'expected 0.0 test_error, got %.2f' % (
    #         test_error,)
