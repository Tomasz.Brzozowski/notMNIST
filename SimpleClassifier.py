import numpy as np

from six.moves import cPickle as pickle
from sklearn import linear_model

pf = pickle.load(open('./notMNIST.pickle', "rb"))

chosen_imgs_train = 50
chosen_imgs_valid = 10000
chosen_imgs_test = 10000

X_train = pf["train_dataset"][:chosen_imgs_train, :, :]
X_train = np.reshape(X_train,(chosen_imgs_train, 28 * 28))
Y_train = pf["train_labels"][:chosen_imgs_train]
X_valid = pf["valid_dataset"][:chosen_imgs_valid, :, :]
X_valid = np.reshape(X_valid,(chosen_imgs_valid, 28 * 28))
Y_valid = pf["valid_dataset"][:chosen_imgs_valid]
X_test = pf["test_dataset"][:chosen_imgs_test, :, :]
X_test = np.reshape(X_test,(chosen_imgs_test, 28 * 28))
Y_test = pf["test_labels"][:chosen_imgs_test]

np.arange()
clf = linear_model.LogisticRegression()
clf.fit(X_train, Y_train)
# print(clf.score(X_valid, Y_valid))
print(clf.score(X_test, Y_test))

