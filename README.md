Project based on Udacity Deep Learning free course.

Prerequisites:
 - Python 3.x (tested on Python 2.7.13)
 - Tensorflow 1.x (tested on Tensorflow 1.4 and 1.5)
 
For more information about the dataset visit: http://yaroslavvb.blogspot.com/2011/09/notmnist-dataset.html
 
For more information about the base project visit: https://www.udacity.com/course/deep-learning--ud730 

