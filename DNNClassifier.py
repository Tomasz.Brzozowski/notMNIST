import numpy as np
import tensorflow as tf
from six.moves import cPickle as pickle
from six.moves import range

pickle_file = 'notMNIST.pickle'
with open(pickle_file, 'rb') as f:
    save = pickle.load(f)
    train_dataset = save['train_dataset']
    train_labels = save['train_labels']
    valid_dataset = save['valid_dataset']
    valid_labels = save['valid_labels']
    test_dataset = save['test_dataset']
    test_labels = save['test_labels']
    del save  # hint to help gc free up memory
    print('Training set', train_dataset.shape, train_labels.shape)
    print('Validation set', valid_dataset.shape, valid_labels.shape)
    print('Test set', test_dataset.shape, test_labels.shape)
image_size = 28
num_labels = 10

# print(train_dataset[0][0:10])

pert_arr = np.random.rand(28,28)/5
# print(pert_arr)

for item in (train_dataset):
    for i in range(28):
        for j in range(28):
            item[i][j] = item[i][j] + pert_arr[i][j]

# print(train_dataset[0][0:10])


def reformat(dataset, labels):
    dataset = dataset.reshape((-1, image_size * image_size)).astype(np.float32)
    # Map 0 to [1.0, 0.0, 0.0 ...], 1 to [0.0, 1.0, 0.0 ...]
    labels = (np.arange(num_labels) == labels[:, None]).astype(np.float32)
    return dataset, labels


train_dataset, train_labels = reformat(train_dataset, train_labels)
valid_dataset, valid_labels = reformat(valid_dataset, valid_labels)
test_dataset, test_labels = reformat(test_dataset, test_labels)
print('Training set', train_dataset.shape, train_labels.shape)
print('Validation set', valid_dataset.shape, valid_labels.shape)
print('Test set', test_dataset.shape, test_labels.shape)


def accuracy(predictions, labels):
    return (100.0 * np.sum(np.argmax(predictions, 1) == np.argmax(labels, 1))
            / predictions.shape[0])

batch_size = 128
v_batch_size = 500
num_input = image_size * image_size
num_neur1 = 2000
num_neur2 = 700
num_neur3 = 250
lambda_ = 0.001
keep_prob = 0.8
init_learn_rate = 0.8
decay_steps = 100
decay_rate = 0.99
graph = tf.Graph()
with graph.as_default():
    initializer = tf.contrib.layers.xavier_initializer(uniform=True, seed=None, dtype=tf.float32)

    # Input data. For the training data, we use a placeholder that will be fed
    # at run time with a training minibatch.
    tf_train_dataset = tf.placeholder(tf.float32, shape=(batch_size, num_input))
    tf_train_labels = tf.placeholder(tf.float32, shape=(batch_size, num_labels))
    # tf_valid_dataset = tf.constant(valid_dataset)
    tf_valid_dataset = tf.placeholder(tf.float32, shape=(v_batch_size, num_input))
    tf_valid_labels = tf.placeholder(tf.float32, shape=(v_batch_size, num_labels))

    tf_test_dataset = tf.constant(test_dataset)
    # Variables.
    weights = {
        'h1': tf.Variable(initializer([num_input, num_neur1])),
        'h2': tf.Variable(initializer([num_neur1, num_neur2])),
        'h3': tf.Variable(initializer([num_neur2, num_neur3])),
        'out': tf.Variable(initializer([num_neur3, num_labels]))
    }
    biases = {
        'b1': tf.Variable(initializer([num_neur1])),
        'b2': tf.Variable(initializer([num_neur2])),
        'b3': tf.Variable(initializer([num_neur3])),
        'out': tf.Variable(initializer([num_labels]))
    }

    def dropout_on_train(layer, if_drop):
        if if_drop:
            return tf.nn.dropout(layer, keep_prob)
        else:
            return layer

    # Training computation.
    def neural_net(x, if_drop):
        # Hidden fully connected layers with num_neur neurons and dropout during training
        layer_1 = tf.nn.relu(tf.matmul(x, weights['h1']) + biases['b1'])
        layer_1 = dropout_on_train(layer_1, if_drop)
        layer_2 = tf.nn.relu(tf.matmul(layer_1, weights['h2']) + biases['b2'])
        layer_2 = dropout_on_train(layer_2, if_drop)
        layer_3 = tf.nn.relu(tf.matmul(layer_2, weights['h3']) + biases['b3'])
        layer_3 = dropout_on_train(layer_3, if_drop)
        # Output fully connected layer with a neuron for each class
        out_layer = tf.matmul(layer_3, weights['out']) + biases['out']
        return out_layer

    logits = neural_net(tf_train_dataset, 1)
    loss = tf.reduce_mean(
        tf.nn.softmax_cross_entropy_with_logits(labels=tf_train_labels, logits=logits)) \
           + lambda_ * (tf.nn.l2_loss(weights['h1']) +
                        tf.nn.l2_loss(weights['h2']) +
                        tf.nn.l2_loss(weights['h3']) +
                        tf.nn.l2_loss(weights['out']))

    # Optimizer.
    global_step = tf.Variable(0)  # count the number of steps taken.
    learning_rate = tf.train.exponential_decay(init_learn_rate, global_step, decay_steps, decay_rate)
    optimizer = tf.train.GradientDescentOptimizer(learning_rate).minimize(loss, global_step=global_step)
    # Predictions for the training, validation, and test data.
    train_prediction = tf.nn.softmax(logits)
    valid_prediction = tf.nn.softmax(neural_net(tf_valid_dataset, 0))
    test_prediction = tf.nn.softmax(neural_net(tf_test_dataset, 0))

config = tf.ConfigProto()
config.gpu_options.allocator_type = 'BFC'

num_steps = 4001
with tf.Session(graph=graph,config = config) as session:
    tf.global_variables_initializer().run()
    print("Initialized")
    for step in range(num_steps):
        # Pick an offset within the training data, which has been randomized.
        # Note: we could use better randomization across epochs.
        offset = (step * batch_size) % (train_labels.shape[0] - batch_size)
        # Generate a minibatch.
        batch_data = train_dataset[offset:(offset + batch_size), :]
        batch_labels = train_labels[offset:(offset + batch_size), :]
        # Prepare a dictionary telling the session where to feed the minibatch.
        # The key of the dictionary is the placeholder node of the graph to be fed,
        # and the value is the numpy array to feed to it.
        feed_dict = {tf_train_dataset: batch_data, tf_train_labels: batch_labels}
        o, l, predictions = session.run(
            [optimizer, loss, train_prediction], feed_dict=feed_dict)
        if (step % 500 == 0):
            print("Minibatch loss at step %d: %f" % (step, l))
            print("Minibatch accuracy: %.1f%%" % accuracy(predictions, batch_labels))

            v_num_steps = int(valid_dataset.shape[0] / v_batch_size)
            sum_valid_acc = 0

            for v_step in range(v_num_steps):
                # batch = mnist.test.next_batch(batch_size)
                valid_offset = (v_step * v_batch_size) % (valid_labels.shape[0] - v_batch_size)
                valid_batch_data = valid_dataset[valid_offset:(valid_offset + v_batch_size), :]
                valid_batch_labels = valid_labels[valid_offset:(valid_offset + v_batch_size), :]
                sum_valid_acc += accuracy(valid_prediction.eval(feed_dict={tf_valid_dataset: valid_batch_data,
                                                        tf_valid_labels: valid_batch_labels}),valid_batch_labels)

            av_valid_acc =  sum_valid_acc/v_num_steps
            print("Validation accuracy: {0:.1f}".format(av_valid_acc))

    print("Test accuracy: %.1f%%" % accuracy(test_prediction.eval(), test_labels))
